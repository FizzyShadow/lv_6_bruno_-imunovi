﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6_Bruno_Šimunović
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
