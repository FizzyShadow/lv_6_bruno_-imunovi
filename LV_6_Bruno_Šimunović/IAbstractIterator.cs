﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_6_Bruno_Šimunović
{
    interface IAbstractIterator
    {
        Note First();
        Note Next();
        bool IsDone { get; }
        Note Current { get; }
    }
}
