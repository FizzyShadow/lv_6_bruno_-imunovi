﻿using System;

namespace LV_6_Bruno_Šimunović
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note = new Note("Druzba Pere Kvrzice", "U jednom malom selu, nepoznatom mnogima, dogodila se zanimljiva avantura");
            Note note1 = new Note("Segrt Hlapic", "Mali mis sa opasnim cizmama");
            Note note2 = new Note("Sturt Mali", "Mali zaigrani mis koji je pronasao novu obitelj");
            Notebook notebook = new Notebook();
            notebook.AddNote(note);
            notebook.AddNote(note1);
            notebook.AddNote(note2);
            IAbstractIterator iterator = notebook.GetIterator();
            for(int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
                
            }
            Console.WriteLine();
          

            iterator.First().Show();

            notebook.RemoveNote(note1);
            iterator = notebook.GetIterator();
            Console.WriteLine();
            Console.WriteLine();
            for (int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }

            notebook.Clear();
            iterator = notebook.GetIterator();
            Console.WriteLine();
            Console.WriteLine(); 
            for (int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
