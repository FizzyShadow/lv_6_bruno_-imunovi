﻿using System;

namespace Zadatak_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product = new Product("Labelo", 50.5);
            Product product1 = new Product("Stol", 1050.5);
            Product product2 = new Product("Vrata", 790.5);
            Box box = new Box();
            box.AddProduct(product);
            box.AddProduct(product1);
            box.AddProduct(product2);
            IAbstractIterator iterator = box.GetIterator();
            for(int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                Console.WriteLine();
                iterator.Next();
            }
            box.RemoveProduct(product1);
            iterator = box.GetIterator();
            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                Console.WriteLine();
                iterator.Next();
            }
        }
    }
}
