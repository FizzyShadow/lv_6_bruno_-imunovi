﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_3
{
    class CareTaker
    {
        public List<Memento> mementos;
        public CareTaker()
        {
            this.mementos = new List<Memento>();
        }
        public void AddMementos(Memento memento)
        {
            mementos.Add(memento);
        }
        public void RemoveMementos(Memento memento)
        {
            mementos.Remove(memento);
        }
        public Memento GetMemento(int index)
        {
            return mementos[index];
        }
        public int Count { get { return this.mementos.Count; } }

    }
}
