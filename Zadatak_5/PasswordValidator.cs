﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zadatak_5
{
    class PasswordValidator
    {
        private StringChecker prvi;
        private StringChecker zadnji;
        public void SetNext(StringChecker next)
        {
            this.zadnji.SetNext(next);
            this.zadnji = next;
        }
        public bool Check(string stringToCheck)
        {
            return prvi.Check(stringToCheck);
        }

        public PasswordValidator(StringChecker prvi)
        {
            this.prvi = prvi;
            this.zadnji = prvi;
        }
    }
}
