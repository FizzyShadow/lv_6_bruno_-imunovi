﻿using System;

namespace Zadatak_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            //FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
            //logger.SetNextLogger(fileLogger);
            //logger.Log("Logiranje", MessageType.ALL);
            //logger.Log("Logiranje", MessageType.WARNING);
            //logger.Log("Logiranje", MessageType.ERROR);
            //logger.Log("Logiranje", MessageType.INFO);


            StringChecker stringChecker = new StringDigitChecker();
            StringLengthChecker stringLengthChecker = new StringLengthChecker();
            StringLowerCaseChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpperCaseChecker = new StringUpperCaseChecker();
            stringChecker.SetNext(stringLowerCaseChecker);
            stringLowerCaseChecker.SetNext(stringLengthChecker);
            stringLengthChecker.SetNext(stringUpperCaseChecker);

            string pero = "Pouka o malome Petru9.";
            string pero1 = "nece proc";
            Console.WriteLine(stringChecker.Check(pero));
            Console.WriteLine(stringChecker.Check(pero1));
            StringChecker stringChecker1 = new StringDigitChecker();

            PasswordValidator passwordValidator = new PasswordValidator(stringChecker1);
            passwordValidator.SetNext(stringLowerCaseChecker);
            passwordValidator.SetNext(stringLengthChecker);
            passwordValidator.SetNext(stringUpperCaseChecker);
            Console.WriteLine(passwordValidator.Check(pero));
            Console.WriteLine(passwordValidator.Check(pero1));6

        }
    }
}
