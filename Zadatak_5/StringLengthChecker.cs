﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Zadatak_5
{
    class StringLengthChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Length > 10;
        }
    }
}
