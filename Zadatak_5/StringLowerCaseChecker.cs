﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Zadatak_5
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            return stringToCheck.Any(char.IsLower);
        }
    }
}
